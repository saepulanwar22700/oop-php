<?php

require('animal.php');
require('Frog.php');
require('Ape.php');

$sheep = new Animal("shaun");

echo "Nama :  $sheep->name <br>"; 
echo "Legs :  $sheep->legs <br>"; 
echo "Cold Blooded :  $sheep->cold_blooded <br><br>"; 

$kodok = new Frog("buduk");

echo "Nama :  $kodok->name <br>"; 
echo "Legs :  $kodok->legs <br>"; 
echo "Cold Blooded :  $kodok->cold_blooded <br>"; 
$kodok->jump(); //hop hop"

$sungokong = new Ape("kera sakti");

echo "<br><br>Nama :  $sungokong->name <br>"; 
echo "Legs :  $sungokong->legs <br>"; 
echo "Cold Blooded :  $sungokong->cold_blooded <br>";
$sungokong->yell(); // "Auooo"
